=== Tap2Pay Payment Gateway Plugin ===
Contributors: Tap2pay Inc.
Donate link: https://tap2pay.me/
Tags: payment gateway, tap2pay, payment widget, accept payments, paypal, cards, payment provider, payment plugin, tap2pay.me payment, woocommerce, woocommerce payment, woocommerce gateway, woocommerce paypal, woocommerce cards 
Requires at least: 3.0.0
Tested up to: 3.3.1
Stable tag: 1

Display the most popular news or reviews about your project in a widget. You could customize styles of widget or choose default styles: horizontal, tiny and default.

== Description ==
Accept wordldwide credit & debit cards, digital wallets, ACH, SEPA Direct Debit, Bitcoin, Ethereum, & Paypal.
Sell on your website, social pages and even by popular messengers.

* Support https://tap2pay.me/contact

= Features =
* Accept Cards, Paypal, Wire Transfer, SEPA, ACH, Direct Debit, Bitcoin and Ethereum.
* Supported payment gateways: Bluesnap, Braintree, Stripe, Paypal, Bepaid, CreditGuard
* Bank's card acquiring
* No install app needed
* One-click payment
* Subscriptions 
* Pay via messengers: Facebook Messenger, WhatsApp, Telegram, WeChat, Viber

== Installation ==
* Download the latest version of the plugin to your computer.
* With an FTP program, access your site's server.
* Upload the plugin folder to the /wp-content/plugins folder.
* In the WordPress administration panels, click on plugins from the menu on the left side.
* You should see the "Tap2Pay Payment Gateway Plugin" plugin listed.
* To turn the plugin on, click "activate" on the bottom of the plugin name.
* You should have now a new menu item called "Tap2Pay Payment Gateway Plugin" in your widget menu.
* Create free account at https://secure.tap2pay.me/users/signup
* Go to [Settings](https://secure.tap2pay.me/merchants/settings) at Tap2pay account and copy API Key and Merchant Id 
* Select tab [Notifications](https://secure.tap2pay.me/merchants/settings/notifications) add Webhook URL `https://YOURDOMAIN.COM?wc-api=wc_tap2pay_payment_gateway`
YOURDOMAIN.COM change, please, to your website domain name.  
* Add Tap2Pay in WooCommerce dashboard and paste API Key and Merchant Id 

= Tested on =

* MySQL 5.1.26 
* Apache 2.2.11
* Linux
* Explorer 8
* Safari 4.1
* Firefox 3.5
* Chrome 5.1
* Opera 9.6

== Frequently Asked Questions ==
* https://tap2pay.me/contacts

== Changelog ==

= 1 =
* Release

== Upgrade Notice ==

== Screenshots ==

1. Screenshot Tap2Pay Payment Widget loaded on your website
2. Screenshot Tap2Pay Dashboard
3. Screenshot Tap2Pay CRM Chat, Support and Sale via popular Messengers: FB, Telegram, Whatsapp, Viber, WeChat
4. Screenshot Tap2Pay Send Invoice in Realtime
5. Screenshot Tap2Pay Selling Tools. You can sell by website, social networks, messengers, QR-codes, advertising